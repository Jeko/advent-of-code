(use-modules
               (jeko-packages)
	       (gnu packages guile)
	       (gnu packages guile-xyz)
	       (guix packages)
	       (guix build-system guile)
	       ((guix licenses) #:prefix license:))

	      (package
	       (name "")
	       (version "")
	       (build-system guile-build-system)
	       (source "")
	       (inputs (list guile-3.0 guile-gunit64))
	       (propagated-inputs (list))
	       (home-page "")
	       (synopsis "")
	       (description "" )
	       (license license:gpl3+))
